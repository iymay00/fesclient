﻿using FESClient.Models;
using FESClient.Models.FESDeviceManaging;
using FESClient.ViewModels;
using FESClient.Views;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FESClient
{
    /// <summary>
    /// Interaction logic for WelcomeWindow.xaml
    /// </summary>
    public partial class WelcomeWindow : Window
    {
        // An instance of the appropriate view model
        private WelcomeWindowViewModel _welcomeWindowViewModel;

        public WelcomeWindow()
        {
            InitializeComponent();

            // Initialize the view's data context
            DataContext = new WelcomeWindowViewModel();
            _welcomeWindowViewModel = DataContext as WelcomeWindowViewModel;
        }

        #region Callback methods 

        private void OnAnyCOMPortSelected(object sender, RoutedEventArgs e)
        {
            // Configure the selected com port
            string portName = COMPortComboBox.SelectedValue.ToString();
            SerialPort port = new SerialPort
            {
                PortName = portName,
                BaudRate = 115200,
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                Handshake = Handshake.None,
                ReadTimeout = 500,
                WriteTimeout = 500
            };

            // Attempt to open the selected COM port
            try
            {
                //port.Open();
            }
            catch (Exception)
            {
                // An invalid serial port was connected. Return to exit the method
                port.Dispose();
                return;
            }

            // If correct, the FES device should have beeped, and the user may continue to the next window.
            // TODO: Write some static method that can simply write to the port and see if we get a beep
            _welcomeWindowViewModel.serialPort = port; 
            WelcomeContinueButton.IsEnabled = true;

            // Create an instance of an FESClientManager with the available serial port
            //_welcomeWindowViewModel.fesDevice = new FESDevice(serialPort);
        }

        private void WelcomeContinueButton_Click(object sender, RoutedEventArgs e)
        {
            // Create and pass data to the next window
            FESClientManager fesClientManager = new FESClientManager(_welcomeWindowViewModel.fesDevice);
            MainWindow mainWindow = new MainWindow(fesClientManager, _welcomeWindowViewModel.serialPort);

            // Show the main window and close the welcome window
            mainWindow.Show();
            this.Close();
        }

        #endregion
    }
}
