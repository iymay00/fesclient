﻿using FESClient.Models;
using FESClient.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace FESClient.Views
{
    /// <summary>
    /// Interaction logic for WaveformSelectionWindow.xaml
    /// </summary>
    public partial class WaveformSelectionWindow : Window
    {
        // An instance of the appropriate view model (in this case, it shares the MainWindow view model)
        private MainWindowViewModel _mainWindowViewModel;

        // A reference to the parent window's view
        private MainWindow _mainWindowView;

        // A delegate that will return the position of the MouseButtonEventArgs event objects
        public delegate Point GetMousePosition(IInputElement element);

        public WaveformSelectionWindow(object dataContext, MainWindow mainWindowView)
        {
            InitializeComponent();

            // Initialize the view's data context
            DataContext = dataContext;
            _mainWindowViewModel = DataContext as MainWindowViewModel;

            // Save the main window view for re-enabling after closing the tuning window 
            _mainWindowView = mainWindowView;
        }

        private void AddRemoveWaveform_Click(object sender, RoutedEventArgs e)
        {
            // Display "Add" on the button instead of "Remove"
            if ((e.Source as Button).Content.ToString() == "Remove")
            {
                // Remove the waveform associated with the button 
                Waveform selectedWaveform = ((FrameworkElement)sender).DataContext as Waveform;
                _mainWindowViewModel.WaveformList.Remove(selectedWaveform);

                // Update the button display text
                (e.Source as Button).Content = "Add";
            }
            else
            {
                // Add the waveform associated with the button 
                Waveform selectedWaveform = ((FrameworkElement)sender).DataContext as Waveform;
                _mainWindowViewModel.WaveformList.Add(selectedWaveform);

                // Update the button display text
                (e.Source as Button).Content = "Remove";
            }
        }

        /// <summary>
        /// Disable the select all button in the top left corner of the datagrid
        /// </summary>
        private void Waveforms_Loaded(object sender, RoutedEventArgs e)
        {
            var waveformListDataGrid = sender as DataGrid;
            var border = VisualTreeHelper.GetChild(waveformListDataGrid, 0) as Border;
            var scrollViewer = VisualTreeHelper.GetChild(border, 0) as ScrollViewer;
            var grid = VisualTreeHelper.GetChild(scrollViewer, 0) as Grid;
            var button = VisualTreeHelper.GetChild(grid, 0) as Button;
            button.IsEnabled = false;
            button.UpdateDefaultStyle();
        }

        /// <summary>
        /// The method that is called upon the tuning window being close. 
        /// </summary>
        private void Window_Closed(object sender, EventArgs e)
        {
            // Eenable the main window
            _mainWindowView.IsEnabled = true;
        }


        #region Helper methods for accessing data grid row index

        private bool MouseOnTargetRow(Visual target, GetMousePosition position)
        {
            Rect positionBounds = VisualTreeHelper.GetDescendantBounds(target);
            Point mousePosition = position((IInputElement)target);
            return positionBounds.Contains(mousePosition);
        }

        private DataGridRow GetDataGridRow(int index)
        {
            if (Waveforms.ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
            {
                return null;
            }
            else
            {
                return Waveforms.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;
            }
        }

        private int GetDataGridItemCurrentRowIndex(GetMousePosition position)
        {
            int currentIndex = -1;
            for (int i = 0; i < Waveforms.Items.Count; i++)
            {
                DataGridRow row = GetDataGridRow(i);
                if (MouseOnTargetRow(row, position))
                {
                    currentIndex = i;
                    break;
                }
            }

            return currentIndex;
        }

        #endregion

        private void Waveforms_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {

        }
    }
}
