﻿using FESClient.Models;
using FESClient.Models.FESDeviceManaging;
using FESClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace FESClient.Views
{
    /// <summary>
    /// Interaction logic for TuningWindow.xaml
    /// </summary>
    public partial class TuningWindow : Window
    {
        // An instance of the appropriate view model (in this case, it shares the MainWindow view model)
        private MainWindowViewModel _mainWindowViewModel;

        // A reference to the parent window's view
        private MainWindow _mainWindowView;

        // The data structure used to tune the stimulation amplitudes of FES channels. 
        // By default all channel amplitudes are 0
        private ObservableCollection<FESChannel> _channelTuningSettings = new ObservableCollection<FESChannel> 
        (
            new FESChannel[Constants.nChannels].Select(c => c = new FESChannel()).ToArray()
        );
        private int _tuningIndex;

        public TuningWindow(object dataContext, MainWindow mainWindowView, int tuningIndex) 
        {
            InitializeComponent();

            // Initialize the view's data context
            DataContext = dataContext;
            _mainWindowViewModel = DataContext as MainWindowViewModel;

            // Save the main window view for re-enabling after closing the tuning window 
            _mainWindowView = mainWindowView;

            // Initialize all channels of the FES to 0 amplitude for tuning 
            _mainWindowViewModel.FESDevice.ChannelSettings = _channelTuningSettings;

            // Save the tuning index for the current tuning session
            _tuningIndex = tuningIndex;
        }

        private void IncrementAmplitude_Click(object sender, RoutedEventArgs e)
        {
            // Increment the value of the current amplitude at the tuning index
            _channelTuningSettings[_tuningIndex].Amplitude += Constants.amplitudeIncrements;

            // Update the FES device to stimulate at the new current amplitude
            _mainWindowViewModel.FESDevice.ChannelSettings = _channelTuningSettings;
        }

        private void DecrementAmplitude_Click(object sender, RoutedEventArgs e)
        {
            // Increment the value of the current amplitude at the tuning index
            _channelTuningSettings[_tuningIndex].Amplitude -= Constants.amplitudeIncrements;

            // Update the FES device to stimulate at the new current amplitude
            _mainWindowViewModel.FESDevice.ChannelSettings = _channelTuningSettings;
        }

        private void SaveTuning_Click(object sender, RoutedEventArgs e)
        {
            // Save the selected amplitude to the waveform list object at the tuning index
            _mainWindowViewModel.WaveformList[_tuningIndex].DefaultAmplitude = _channelTuningSettings[_tuningIndex].Amplitude;

            // Close the window
            this.Close();
        }

        private void PauseResumeTuning_Click(object sender, RoutedEventArgs e)
        {
            // Display "Resume Tuning" on the button instead of "Pause Tuning"
            if (PauseResumeTuning.Content.ToString() == "Pause Tuning")
            {
                // Stop the FES from stimulating
                 _mainWindowViewModel.FESDevice.Pause();

                // Update the button display text
                PauseResumeTuning.Content = "Resume Tuning";
            }
            else
            {
                // Unpause the FES from stimulating
                _mainWindowViewModel.FESDevice.Resume();

                // Update the button display text
                PauseResumeTuning.Content = "Pause Tuning";
            }
        }

        private void CancelTuning_Click(object sender, RoutedEventArgs e)
        {
            // Close the tuning window
            this.Close();
        }

        /// <summary>
        /// The method that is called upon the tuning window being close. This method is responsible for pausing the FES
        /// device after a tuning has been finished (cancelled or saved).
        /// </summary>
        private void Window_Closed(object sender, EventArgs e)
        {
            // Ensure that the FES is not stimulating
            _mainWindowViewModel.FESDevice.Pause();

            // Eenable the main window
            _mainWindowView.IsEnabled = true;
        }
    }
}
