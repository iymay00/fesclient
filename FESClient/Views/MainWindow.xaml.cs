﻿using FESClient.Models;
using FESClient.ViewModels;
using System.IO.Ports;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace FESClient.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // An instance of the appropriate view model
        private MainWindowViewModel _mainWindowViewModel;

        // A delegate that will return the position of the DragDropEventArgs and the MouseButtonEventArgs event objects
        public delegate Point GetDragDropPosition(IInputElement element);

        // A view-specific variable for keeping track of the data grid index 
        private int _previousDataGridRowIndex = -1;

        public MainWindow(FESClientManager fesClientManager, SerialPort serialPort)
        {
            InitializeComponent();

            // Initialize the view's data context
            DataContext = new MainWindowViewModel(serialPort);
            _mainWindowViewModel = DataContext as MainWindowViewModel;

            // List box item container style definition
            Style dataGridCellContainerStyle = new Style(typeof(DataGridCell));
            dataGridCellContainerStyle.Setters.Add(new Setter(DataGridCell.AllowDropProperty, true));
            dataGridCellContainerStyle.Setters.Add(new EventSetter(DataGridCell.PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(waveformListDataGrid_PreviewMouseLeftButtonDown)));
            dataGridCellContainerStyle.Setters.Add(new EventSetter(DataGridCell.DropEvent, new DragEventHandler(dataGrid_Drop)));
            WaveformList.CellStyle = dataGridCellContainerStyle;
        }

        #region Callback Functions 

        /// <summary>
        /// Automatically number the waveform list rows
        /// </summary>
        private void WaveformList_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        /// <summary>
        /// Disable the select all button in the top left corner of the datagrid
        /// </summary>
        private void WaveformList_Loaded(object sender, RoutedEventArgs e)
        {
            var waveformListDataGrid = sender as DataGrid;
            var border = VisualTreeHelper.GetChild(waveformListDataGrid, 0) as Border;
            var scrollViewer = VisualTreeHelper.GetChild(border, 0) as ScrollViewer;
            var grid = VisualTreeHelper.GetChild(scrollViewer, 0) as Grid;
            var button = VisualTreeHelper.GetChild(grid, 0) as Button;
            button.IsEnabled = false;
            button.UpdateDefaultStyle();
        }

        private void waveformListDataGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _previousDataGridRowIndex = GetDataGridItemCurrentRowIndex(e.GetPosition);

            if (_previousDataGridRowIndex < 0)
            {
                return;
            }

            WaveformList.SelectedIndex = _previousDataGridRowIndex;
            Waveform selectedRow = WaveformList.Items[_previousDataGridRowIndex] as Waveform;

            if (selectedRow == null)
            {
                return;
            }

            DragDropEffects dragDropEffects = DragDropEffects.Move;
            if (DragDrop.DoDragDrop(WaveformList, selectedRow, dragDropEffects) != DragDropEffects.None)
            {
                WaveformList.SelectedItem = selectedRow;
            }
        }

        void dataGrid_Drop(object sender, DragEventArgs e)
        {
            if (_previousDataGridRowIndex < 0)
            {
                return;
            }

            int targetIndex = this.GetDataGridItemCurrentRowIndex(e.GetPosition);

            if (targetIndex < 0)
            {
                return;
            }

            if (targetIndex == _previousDataGridRowIndex)
            {
                return;
            }

            // Invoke the view model's object swap method 
            _mainWindowViewModel.Swap(_previousDataGridRowIndex, targetIndex);
        }

        void TrashCan_Drop(object sender, DragEventArgs e)
        {
            // Get the index of the object being dragged
            Waveform droppedObject = e.Data.GetData(typeof(Waveform)) as Waveform;
            int removedIdx = WaveformList.Items.IndexOf(droppedObject);

            // Invoke the view model's object remove method
            _mainWindowViewModel.Remove(removedIdx);

            // Update the row numberings 
            WaveformList.Items.Refresh();
        }

        private void Tuner_Drop(object sender, DragEventArgs e)
        {
            // Get the index of the object being dragged
            Waveform droppedObject = e.Data.GetData(typeof(Waveform)) as Waveform;
            int tuningIdx = WaveformList.Items.IndexOf(droppedObject);

            // Disable the main window, unpause the FES device, and open up the Tuner pop-up window
            this.IsEnabled = false;
            _mainWindowViewModel.FESDevice.Resume();
            TuningWindow tuningWindow = new TuningWindow(DataContext, this, tuningIdx);
            tuningWindow.Show();
        }

        private void AddWaveform_Click(object sender, RoutedEventArgs e)
        {
            // Disable the main window, Waveform Selector pop-up window
            this.IsEnabled = false;
            WaveformSelectionWindow waveformSelectionWindow = new WaveformSelectionWindow(DataContext, this);
            waveformSelectionWindow.Show();
        }

        private void ResetConfigurationButton_Click(object sender, RoutedEventArgs e)
        {
            // Clear the view model's waveform list
            _mainWindowViewModel.WaveformList.Clear();
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            // Open the connection to the NRPA 
            _mainWindowViewModel.NRPA.Connect();

            // Disable the button 
            (e.Source as Button).IsEnabled = false;

            // Show the connection bar as "connected"
            ConnectionBar.IsIndeterminate = true;
        }

        private void EmergencyStopButton_Click(object sender, RoutedEventArgs e)
        {
            _mainWindowViewModel.NRPA.Stop();
        }

        #endregion

        #region Helper methods for accessing data grid row index

        private bool MouseOnTargetRow(Visual target, GetDragDropPosition position)
        {
            Rect positionBounds = VisualTreeHelper.GetDescendantBounds(target);
            Point mousePosition = position((IInputElement)target);
            return positionBounds.Contains(mousePosition);
        }

        private DataGridRow GetDataGridRow(int index)
        {
            if (WaveformList.ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
            {
                return null;
            }
            else
            {
                return WaveformList.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;
            }
        }

        private int GetDataGridItemCurrentRowIndex(GetDragDropPosition position)
        {
            int currentIndex = -1;
            for (int i = 0; i < WaveformList.Items.Count; i++)
            {
                DataGridRow row = GetDataGridRow(i);
                if (MouseOnTargetRow(row, position))
                {
                    currentIndex = i;
                    break;
                }
            }

            return currentIndex;
        }


        #endregion
    }
}
