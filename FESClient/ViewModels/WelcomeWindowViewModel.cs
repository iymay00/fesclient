﻿using FESClient.Models;
using FESClient.Models.FESDeviceManaging;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESClient.ViewModels
{
    class WelcomeWindowViewModel
    {
        // A container for displaying all available COM ports on the PC
        public IEnumerable<string> AvailableCOMports { get; }

        // The FES client objects
        public FESDevice fesDevice { get; set; }

        // The serial port that will be used to communicate with the FES Device
        public SerialPort serialPort;

        public WelcomeWindowViewModel()
        {
            AvailableCOMports = SerialPort.GetPortNames();
        }
    }
}
