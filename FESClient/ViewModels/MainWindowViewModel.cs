﻿using FESClient.Models;
using FESClient.Models.FESDeviceManaging;
using FESClient.Models.NRPAEmulation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESClient.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        // Declare the property changed event that will notify the view of property changes
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        // The FES device that the FES Client drives
        public FESDevice FESDevice;

        // The channel settings of the FES device 
        private ObservableCollection<FESChannel> _FESChannelSettings;
        public ObservableCollection<FESChannel> FESChannelSettings
        {
            get
            {
                return _FESChannelSettings;
            }
            set
            {
                _FESChannelSettings = value;
                NotifyPropertyChanged(nameof(FESChannelSettings));
            }
        }

        // The waveform list that will be used in the activity 
        private ObservableCollection<Waveform> _waveformList;
        public ObservableCollection<Waveform> WaveformList
        {
            get
            {
                return _waveformList;
            }
            set
            {
                _waveformList = value;
                NotifyPropertyChanged(nameof(WaveformList));
            }
        }

        // The list of available waveforms that can be used in the activity 
        private ObservableCollection<Waveform> _availableWaveforms;
        public ObservableCollection<Waveform> AvailableWaveforms
        {
            get
            {
                return _availableWaveforms;
            }
            set
            {
                _availableWaveforms = value;
                NotifyPropertyChanged(nameof(AvailableWaveforms));
            }
        }

        // The FES client's NRPA emulator
        public NRPA NRPA;

        // The FES client's IP address
        private string _ipAddress;
        public string IPAddress
        {
            get
            {
                return _ipAddress;
            }
            set
            {
                _ipAddress = value;
                NotifyPropertyChanged(nameof(IPAddress));
            }
        }

        /// <summary>
        /// View model constructor
        /// </summary>
        public MainWindowViewModel(SerialPort serialPort)
        {
            // Set up the FES device object
            FESChannelSettings = new ObservableCollection<FESChannel>
            (
                new FESChannel[Constants.nChannels].Select(c => c = new FESChannel()).ToArray()
            );
            FESDevice = new FESDevice(serialPort, FESChannelSettings);

            // The waveform list is filled with zero waveforms on startup
            WaveformList = new ObservableCollection<Waveform>();
            while (WaveformList.Count < Constants.nWaveformes)
            {
                WaveformList.Add(new Waveform());
            }

            // Set the IP Address 
            IPAddress = Constants.defaultIPAddress;

            // Initialize the NRPA emulator to use the View Model's data structures
            NRPA = new NRPA(FESDevice, WaveformList, IPAddress);    

            // Subscribe view model's Waveform list and FESChannelSettings to receive notifications model object changes
            NRPA.WaveformList.CollectionChanged += onNRPAWaveformListUpdated;
            NRPA.PropertyChanged += onChannelSettingsUpdated;

            // Populate the list of available waveforms
            AvailableWaveforms = new ObservableCollection<Waveform>();
            foreach (string functionality in Constants.startupFunctionalities)
            {
                AvailableWaveforms.Add(new Waveform(functionality));
            }
        }

        public void onNRPAWaveformListUpdated(object sender, NotifyCollectionChangedEventArgs e)
        {
            // Update the view model's Waveform List to Match that of the NRPA Model 
            WaveformList = NRPA.WaveformList;
        }

        public void onChannelSettingsUpdated(object sender, PropertyChangedEventArgs e)
        {
            // Update the FES device's channel settings 
            FESDevice.ChannelSettings = NRPA.ChannelSettings;

            // Update the view model's FES representation to show the current state of the FES device
            FESChannelSettings = FESDevice.ChannelSettings;
        }

        /// <summary>
        /// A method to swap two entries in the Objects OberservableCollection
        /// </summary>
        /// <param name="index1"> The index of the first entry to be swapped. </param>
        /// <param name="index2"> The index of the second entry to be swapped. </param>
        public void Swap(int index1, int index2)
        {
            // Perform the swap of entries
            Waveform temp = WaveformList[index1];
            WaveformList[index1] = WaveformList[index2];
            WaveformList[index2] = temp;
        }

        /// <summary>
        /// A method to remove an entry from the Objects ObservableCollection
        /// </summary>
        /// <param name="index"> The index of the entry to be removed. </param>
        public void Remove(int index)
        {
            WaveformList.RemoveAt(index);
        }
    }
}
