﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESClient.Utilities
{
    static class FESUtilities
    {

        /// <summary>
        /// A method to convert a number represented in binary (as a string) to a set of bytes (an array of unsigned 8-bit integers)
        /// </summary>
        /// <param name="binaryString"> The binary string representing the configuration settings for the FES device. </param>
        /// <returns> The byte array </returns>
        public static byte[] ToByteArray(string binaryString)
        {
            int numOfBytes = binaryString.Length / 8;
            byte[] byteArray = new byte[numOfBytes];
            for (int i = 0; i < numOfBytes; ++i)
            {
                byteArray[i] = Convert.ToByte(binaryString.Substring(8 * i, 8), 2);
            }
            return byteArray;
        }

        public static string ComposeBitstring(int[] pulseCurrents, int[] pulseWidths)
        {
            // Singlets (single pulses for now)
            int[] pulseModes = new int[pulseWidths.Length];

            // Build checksum
            int checksum = (pulseModes.Sum() + pulseWidths.Sum() + pulseCurrents.Sum()) % 32;

            // Convert to binary strings (bitstrings)
            string checkstring = Convert.ToString(checksum, 2).PadLeft(5, '0');
            string channelsConfigurationsBitstring = "";

            // Construct the binary string containing each channel's parameters
            for (int i = 0; i < pulseWidths.Length; i++)
            {
                string modestring = Convert.ToString(pulseModes[i], 2).PadLeft(2, '0');
                string pulsewidthstring = Convert.ToString(pulseWidths[i], 2).PadLeft(9, '0');
                string pulsecurrentstring = Convert.ToString(pulseCurrents[i], 2).PadLeft(7, '0');

                // Set the 3 configuation bytes for the current channel
                string channelConfigurationBytes = "0" + 
                                                    modestring + 
                                                    "000" + 
                                                    pulsewidthstring.Substring(0, 2) + 
                                                    "0" + 
                                                    pulsewidthstring.Substring(2, 7) + 
                                                    "0" + 
                                                    pulsecurrentstring;

                // Concatinate the configurations bitstring 
                channelsConfigurationsBitstring += channelConfigurationBytes;
            }

            // Compose the completed bitstring
            return "101" + checkstring + channelsConfigurationsBitstring;
        }
    }
}
