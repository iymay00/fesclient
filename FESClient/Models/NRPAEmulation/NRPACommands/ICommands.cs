﻿namespace FESClient.Models.NRPAEmulation.NRPACommands
{
    interface ICommands
    {
        // The following command interfaces are supported by the NRPA
        void BatchUpdate();
        void Initialize();
        void ToggleTherapy();
        void ModifyAmplitude();
        void ModifyPulsewidth();
        void UpdateWaveformtable();
    }
}
