﻿using System.Collections.Generic;

namespace FESClient.Models.NRPAEmulation.JSONParsing
{
    public class WaveformtableEntryListParser
    {
        public List<WaveformtableEntryParser> Entries { get; set; }
    }

    public class WaveformtableEntryParser
    {
        public int Duration { get; set; }
        public int Enables { get; set; }
        public int Frequency { get; set; }
    }
}
