﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESClient.Models.NRPAEmulation.JSONParsing
{
    /// <summary>
    /// Classes for parsing the list of waveforms from a JSON-formatted http request
    /// </summary>
    public class WaveformListParser
    {
        public List<WaveformParser> Entries { get; set; }
    }

    public class WaveformParser
    {
        public List<string> electrodes { get; set; }
        public float amplitude { get; set; }
        public int pulseWidth { get; set; }
    }
}
