﻿using FESClient.Models.FESDeviceManaging;
using FESClient.Models.Logging;
using FESClient.Models.NRPAEmulation.HttpCommunication;
using FESClient.Models.NRPAEmulation.JSONParsing;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

namespace FESClient.Models.NRPAEmulation
{
    class NRPA : INotifyPropertyChanged
    {
        // Declare the property changed event that will notify the view of property changes
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        // The NRPA has access to the application's logger
        Logger NRPALogger;

        // The NRPA emulator has a stim table. The stim table has 90 stim lines (0 indexed).
        private StimulationLine[] _stimulationTable;

        // When started, the NRPA executes a task that repeatedly loops through the stimulaiton table
        private Thread _stimulationTableCycleThread;

        // The NRPA keeps track of the active stimulation table line. If the run now token is thrown, do not wait for the stim line's duration.
        private int _runIndex = 0;
        private CancellationTokenSource _runNowTokenSource;

        // The NRPA has a waveform list containing the ten waveforms that can be used for FES stimulation patterns
        public ObservableCollection<Waveform> WaveformList { get; set; }

        // The NRPA emulator has a reference to the FES device in order to communicate FES Commands
        private FESDevice FESDevice;

        // The NRPA has a list of channel settings that will be transmitted to the FES device
        private ObservableCollection<FESChannel> _ChannelSettings;
        public ObservableCollection<FESChannel> ChannelSettings
        {
            get
            {
                return _ChannelSettings;
            }
            set
            {
                _ChannelSettings = value;
                NotifyPropertyChanged(nameof(ChannelSettings));
            }
        }

        // The NRPA implements an http server to communicate with GDL/GDP
        public HttpServer Server { get; set; }

        // A flag to control whether or not the therapy is running
        private bool IsTherapyOn = false;

        /// <summary>
        /// Custom constructor
        /// </summary>
        /// <param name="fesDevice"> A reference to the FES device in order to communicate FES commands. </param>
        /// <param name="waveformList"> The waveform list to be used by the NRPA. </param>
        /// <param name="ipAddress"> The IP Address of the server. </param>
        public NRPA(FESDevice fesDevice, ObservableCollection<Waveform> waveformList, string ipAddress)
        {
            NRPALogger = new Logger();

            // Set the NRPA's fes device and channel settings
            this.FESDevice = fesDevice;
            ChannelSettings = this.FESDevice.ChannelSettings;

            // The NRPA has a stimulation table of 90 lines which are all zero lines by default
            _stimulationTable = new StimulationLine[Constants.nStimulationLines].Select(l => l = new StimulationLine())
                                                                                .ToArray();

            // Set the NRPA waveform table 
            WaveformList = waveformList;

            // Instantiate the NRPA server 
            Server = new HttpServer(ipAddress);

            // Subscribe the NRPA to receive notifications upon new http requests made its server
            Server.PropertyChanged += onHttpRequestReceived;

            // Set up the line delay's cancellation token (allows the implementation of runNow)
            _runNowTokenSource = new CancellationTokenSource(); 
        }

        /// <summary>
        /// Start running the NRPA emulator's server
        /// </summary>
        public void Connect()
        {
            // Start the http server
            Server.startListening();
        }

        /// <summary>
        ///  Stop running the NRPA emulator
        /// </summary>
        public void Stop()
        {
            // Stop the FES Device
            FESDevice.Stop();

            // Stop the http server
            Server.stopListening();

            // Stop cycling through the NRPA stimulation table 
            _stimulationTableCycleThread.Abort();
        }

        /// <summary>
        /// A method to interpret http server requests into a stimulation table object
        /// </summary>
        /// <param name="request"> A reaw Http Listener Request</param>
        /// <returns> An update Stimulation Table</returns>
        private void Interpret(HttpListenerRequest request)
        {
            // Split the http request into the command part and the parameters part to simplify parsing
            string requestString = request.Url.ToString();
            NRPALogger.WriteLog(requestString);

            string command = ExtractCommand(requestString);
            NameValueCollection parameters = ExtractParameters(requestString);

            // Update the Stimulation Table (if applicable) and/or the Waveform Table.
            if (command.Contains("batch"))
            {
                // Batch commands are not supported
                throw new Exception();
            }
            else if (command.Contains("initialize"))
            {
                // Deserialze the JSON waveform table entry list into a list of WaveformTableEntry objects
                WaveformtableEntryListParser parsedWaveformTableEntryList = JsonConvert.DeserializeObject<WaveformtableEntryListParser>(parameters["table"]);

                // Deserialze the JSON waveforms list into a list of Waveform objects
                WaveformListParser parsedWaveformList = JsonConvert.DeserializeObject<WaveformListParser>(parameters["waveforms"]);

                // Set all entries of the waveform list and the stimulation table to their default values
                WaveformList.ToList().ForEach(waveform => {
                                                            waveform.DefaultAmplitude = 0;
                                                            waveform.PulseMode = 0;
                                                            waveform.PulseWidth = 0;
                                                          });
                Array.ForEach(_stimulationTable, stimulationLine => stimulationLine = new StimulationLine());

                // Update the waveform list according to the parameters of the parsed http request
                int waveformListIndex = 0;
                while (parsedWaveformList.Entries?.Any() == true)
                {
                    // Update each waveform in the waveform list
                    WaveformList[waveformListIndex].DefaultAmplitude = (int) parsedWaveformList.Entries[0].amplitude;
                    WaveformList[waveformListIndex].PulseWidth = parsedWaveformList.Entries[0].pulseWidth;

                    // Pop from the front list of new parameters
                    parsedWaveformList.Entries.RemoveAt(0);

                    // Update the line index 
                    waveformListIndex++;
                }

                // Update the stimulation table according to the parameters of the parsed http request
                int writeIndex = 0;
                while (parsedWaveformTableEntryList.Entries?.Any() == true)
                {
                    // Update the stimulation table parameters for each line
                    _stimulationTable[writeIndex].Duration = parsedWaveformTableEntryList.Entries[0].Duration;
                    _stimulationTable[writeIndex].Frequency = parsedWaveformTableEntryList.Entries[0].Frequency;
                    _stimulationTable[writeIndex].Enables = Convert.ToString(parsedWaveformTableEntryList.Entries[0].Enables, 2)
                                                                   .PadLeft(8, '0')
                                                                   .Select(s => s.Equals('1'))
                                                                   .ToArray();

                    // Pop from the front list of new parameters
                    parsedWaveformTableEntryList.Entries.RemoveAt(0);

                    // Update the line index 
                    writeIndex++;
                }
            }
            else if (command.Contains("therapy"))
            {
                // Get the command state 
                string state = parameters["state"];

                // Start or stop the stim
                switch (state)
                {
                    case "on":
                        {
                            // Begin cycling through the NRPA stimulation table 
                            FESDevice.Resume();

                            // When started, the NRPA loops through the stimulation table 
                            // and sends timed commands to the FES device
                            IsTherapyOn = true;
                            _stimulationTableCycleThread = new Thread(StimulationTableCycle);
                            _stimulationTableCycleThread.Start();
                            break;
                        }

                    case "off":
                        {
                            FESDevice.Pause();
                            IsTherapyOn = false;
                            break;
                        }
                }
            }
            else if (command.Contains("modifyamplitude"))
            {
                // Convert the waveform number and amplitude to integers
                int waveformIndex = Convert.ToInt32(parameters["waveform"]) - 1;
                int amplitude = (int) Convert.ToDouble(parameters["amplitude"]);

                // Modify the stim amplitude for a particular waveform
                WaveformList[waveformIndex].DefaultAmplitude = amplitude;
            }
            else if (command.Contains("modifypulsewidth"))
            {
                // Convert the waveform number and pulsewidth to integers
                int waveformIndex = Convert.ToInt32(parameters["waveform"]) - 1;
                int pulsewidth = Convert.ToInt32(parameters["pulsewidth"]);

                // Modify the stim pulsewidth for the specified waveform
                WaveformList[waveformIndex].PulseWidth = pulsewidth;
            }
            else if (command.Contains("updatewaveformtable"))
            {
                // Extract the write index
                int writeIndex = Convert.ToInt32(parameters["writeIndex"]);
                
                // Deserialze the JSON waveform table entry into a WaveformTableEntry object
                WaveformtableEntryListParser parsedWaveformTableEntryList = JsonConvert.DeserializeObject<WaveformtableEntryListParser>(parameters["table"]);

                // Update the stimulation line table
                while (parsedWaveformTableEntryList.Entries?.Any() == true)
                {
                    // Update the stimulation table parameters for each line
                    _stimulationTable[writeIndex].Duration = parsedWaveformTableEntryList.Entries[0].Duration;
                    _stimulationTable[writeIndex].Frequency = parsedWaveformTableEntryList.Entries[0].Frequency;
                    _stimulationTable[writeIndex].Enables = Convert.ToString(parsedWaveformTableEntryList.Entries[0].Enables, 2)
                                                                   .PadLeft(8, '0')
                                                                   .Select(s => s.Equals('1'))
                                                                   .ToArray();

                    // Pop from the front list of new parameters
                    parsedWaveformTableEntryList.Entries.RemoveAt(0);

                    // Update the line index 
                    writeIndex++;
                }

                // Update the NRPA's run index
                _runIndex = Convert.ToInt32(parameters["runIndex"]);

                // Parse for MSB of runIdx
                int runNow = (_runIndex & 0x80) >> 7;

                // If MSB == 1 throw a cancellation to LinePulseTimer 
                if (runNow == 1)
                {
                    // Interrupt the stim table cycling thread in order to immediately move to the new line
                    _stimulationTableCycleThread.Interrupt();

                    // Remove the MSB 
                    _runIndex -= 0x80;
                }
            }
        }

        /// <summary>
        /// A method to extract the command portion of an http string formatted for the NRPA
        /// </summary>
        /// <param name="request"> The full http command string </param>
        /// <returns> The NRPA command</returns>
        private string ExtractCommand(string request)
        {
            // Find the bounds of the NRPA command
            int commandEndIdx = request.LastIndexOf("?");
            int commandStartIdx = request.LastIndexOf("/");

            // Return the command string
            string command = request.Substring(commandStartIdx + 1, commandEndIdx - commandStartIdx - 1);
            return command;
        }

        /// <summary>
        /// A method to extract the parameters portion of an http string formatted for the NRPA
        /// </summary>
        /// <param name="request"> The full http command string </param>
        /// <returns> A dictionary containing the paremeter names and their values. </returns>
        private NameValueCollection ExtractParameters(string request)
        {
            // Discard all parts of the http request that don't contain the command parameters
            var parameterString = request.Substring(request.LastIndexOf("?") + 1);

            // Parse the parameters string into a dictionary data structure
            NameValueCollection parameters = HttpUtility.ParseQueryString(parameterString);
            return parameters;
        }

        /// <summary>
        /// The NRPA generates FES commands that are sent to the FES device 
        /// </summary>
        private void UpdateChannelSettings()
        {
            // Create the FES Channels output array (Initialize the structure to have 0 amplitudes at first)
            FESChannel[] updatedChannelSettings = new FESChannel[Constants.nChannels].Select(c => c = new FESChannel()).ToArray();

            // Generate the correct settings for each FES channel
            for (int i = 0; i < Constants.nChannels; i++)
            {
                // Each channel takes the amplitude of the corresponding waveform. 
                updatedChannelSettings[i].Amplitude = WaveformList[i].DefaultAmplitude;
                updatedChannelSettings[i].PulseWidth = WaveformList[i].PulseWidth;

                // If the channel is disabled, its amplitude is set to 0 
                if (_stimulationTable[_runIndex].Enables[Constants.nChannels - 1 - i] == false)
                {
                    updatedChannelSettings[i].Amplitude = 0;
                }
            }

            // Updates the FES channel settings. This will notify the view model (will route commands to the device).
            ChannelSettings = new ObservableCollection<FESChannel>(updatedChannelSettings);
        }

        private void StimulationTableCycle()
        {
            // Always start at the first line of the stimulation table
            _runIndex = 0;

            // Continuously loop through the stim table
            while (IsTherapyOn)
            {
                // If the stim table bounds have been exceeded, restart the index
                if (_runIndex >= Constants.nStimulationLines)
                {
                    _runIndex = 0;
                }

                // If a zero-line is reached, restart the index and do not send a command
                if (_stimulationTable[_runIndex].Duration == 0)
                {
                    _runIndex = 0;
                    continue;
                }

                // Otherwise update the FES device channel settings with the values of the current stim line
                UpdateChannelSettings();
                 
                try
                {
                    // Move to the next line in the stimulation table after waiting for the stim line duration
                    Thread.Sleep(_stimulationTable[_runIndex].Duration);
                    _runIndex++;
                }
                catch (ThreadInterruptedException ex)
                {
                    // The runNow bit was received in an http request. 
                }           
            }
        }

        private void onHttpRequestReceived(object sender, PropertyChangedEventArgs e)
        {
            // Interpret the current http request and update the stimulation table + waveform list
            Interpret(Server.HttpRequest);
        }
    }
}
