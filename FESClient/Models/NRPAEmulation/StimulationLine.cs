﻿using System.Linq;

namespace FESClient.Models.NRPAEmulation
{
    class StimulationLine
    {
        // A stim line has a duration
        public int Duration { get; set; }

        // A stim line has a stimulation frequency
        public int Frequency {get; set;}

        // A stim line has an enables table specifying which waveforms (channels in the case of FES) are active.
        public bool[] Enables;

        /// <summary>
        /// Default constructor
        /// </summary>
        public StimulationLine()
        {
            // Initialize the duration, frequency, and enables to their default values 
            Duration = Constants.defaultStimulationLineDuration;
            Frequency = Constants.defaultStimulationLineFrequency;
            Enables = new bool[Constants.nChannels].Select(c => c = Constants.defaultWaveformEnableState).ToArray();
        }

        /// <summary>
        /// Custom constructor allowing full specification of a new stimulation line
        /// </summary>
        public StimulationLine(int duration, int frequency, bool[] enables)
        {
            this.Duration = duration;
            this.Frequency = frequency;
            this.Enables = enables;
        }
    }
}
