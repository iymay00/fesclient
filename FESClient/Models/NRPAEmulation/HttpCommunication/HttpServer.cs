﻿using System;
using System.ComponentModel;
using System.Net;
using System.Text;
using System.Windows;

namespace FESClient.Models.NRPAEmulation.HttpCommunication
{
    /// <summary>
    /// This is code based on that of the FakeNRPA.
    /// </summary>
    class HttpServer : INotifyPropertyChanged
    {
        // Declare the property changed event that will notify the view of property changes
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private bool listening = false;
        private bool ready = true;
        private string IPAddress { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public HttpServer(string IPAddress)
        {
            this.IPAddress = IPAddress;
        }

        private App currentApp = (App)Application.Current;

        // The current Http Request. The observer pattern is used to notify the HttpDecoder of new http requests
        private HttpListenerRequest _httpRequest;
        public HttpListenerRequest HttpRequest
        {
            get
            {
                return _httpRequest;
            }
            set
            {
                _httpRequest = value;
                NotifyPropertyChanged(nameof(HttpRequest));
            }
        }

        public void startListening()
        {
            listening = true;

            if (ready)
            {
                startHttpServer();
            }
        }

        public void stopListening()
        {
            listening = false;
        }

        public async void startHttpServer()
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }
            // Create a listener.
            HttpListener listener = new HttpListener();
            // Add the prefix.
            //here the ipaddress should be whatever is in the box in mainwindow
            string s = "http://" + IPAddress + ":9090/";
            listener.Prefixes.Add(s);
            listener.Start();
            ready = false;
            Console.WriteLine("Listening...");
            while (listening)
            {
                // Await a new http listener request to be received. Subscribers will be notified of this
                HttpListenerContext context = await listener.GetContextAsync();
                HttpRequest = context.Request;

                // Obtain a response object.
                HttpListenerResponse response = context.Response;

                // Construct a response.
                //Here a response must be taken in from the controller (success or fail)
                string responseString = "{\"ExecutionTime\":00,\"Id\":0,\"Message\":\"FAKE NRPA: SUCCESSFULLY RECEIVED MESSAGE\",\"PerfCounters\":[],\"QueueCount\":0,\"Status\":0,\"SuccessfulIds\":[1]}";
                byte[] buffer = Encoding.UTF8.GetBytes(responseString);
                // Get a response stream and write the response to it.
                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                // You must close the output stream.
                output.Close();
            }
            listener.Stop();
            ready = true;
        }
    }
}
