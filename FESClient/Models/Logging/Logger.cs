﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FESClient.Models.Logging
{
    class Logger
    {
        // The logger's current message 
        private string _log;
        public string Log { get; set; }

        // The stream writer used to write the log files
        private StreamWriter LogWriter;

        public Logger()
        {
            // Set the log writer to log a new file to the log directory
            LogWriter = new StreamWriter(ConstructDirectoryString( "log" + ".txt"), true);
            LogWriter.AutoFlush = true;

            // Write the header in preparation to log 
            WriteHeader();
        }

        /// <summary>
        /// The message log manager's implementation of the WriteHeader() method
        /// </summary>
        private void WriteHeader()
        {
            LogWriter.Write(("Software version: " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + '\n').ToCharArray());
        }

        /// <summary>
        /// The message log manager's implementation of the Log() method.
        /// </summary>
        /// <param name="message">
        /// Optional parameter. Pass this parameter alone to log a string without needing to pass an LogArgs object.
        /// </param>
        public void WriteLog(string message)
        {
            string time = DateTime.Now.ToString();
            string log = time + "\t" + message;
            LogWriter.WriteLine(log);
        }

        public string ConstructDirectoryString(string directory)
        {
            string projectDirectory = Directory.GetParent(Path.GetDirectoryName(GetType().Assembly.Location)).Parent.FullName;
            string path = Path.Combine(projectDirectory, Constants.logPath + directory);
            return path;
        }
    }
}


