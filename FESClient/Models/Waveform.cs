﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESClient.Models
{
    class Waveform : INotifyPropertyChanged
    {
        // Declare the property changed event that will notify the view of property changes
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string Functionality { get; set; }

        // The minimum amplitude that the waveform may take during the activity
        private int _minAmplitude;
        public int MinAmplitude
        {
            get
            {
                return _minAmplitude;
            }
            set
            {
                if (value < Constants.minAmplitude)
                {
                    _minAmplitude = Constants.minAmplitude;
                }
                else
                {
                    _minAmplitude = value;
                }

                NotifyPropertyChanged(nameof(MinAmplitude));
            }
        }

        // The max amplitude that the waveform may take during the activity
        private int _maxAmplitude;
        public int MaxAmplitude
        {
            get
            {
                return _maxAmplitude;
            }
            set
            {
                if (value > Constants.maxAmplitude)
                {
                    _maxAmplitude = Constants.maxAmplitude;
                }
                else
                {
                    _maxAmplitude = value;
                }

                NotifyPropertyChanged(nameof(MaxAmplitude));
            }
        }

        // When the current amplitude is updated, the min and max amplitudes should be updated according to the auto range width
        private int _defaultAmplitude;
        public int DefaultAmplitude
        {
            get
            {
                return _defaultAmplitude;
            }
            set
            {
                if (value < Constants.minAmplitude)
                {
                    _defaultAmplitude = Constants.minAmplitude;
                }
                else if (value > Constants.maxAmplitude)
                {
                    _defaultAmplitude = Constants.maxAmplitude;
                }
                else
                {
                    _defaultAmplitude = value;
                }
                NotifyPropertyChanged(nameof(DefaultAmplitude));

                // Auto range the min and max amplitudes 
                MinAmplitude = _defaultAmplitude - (Constants.amplitudeRange / 2);
                MaxAmplitude = _defaultAmplitude + (Constants.amplitudeRange / 2);              
            }
        }

        // The waveform pulsewidth
        public int PulseWidth { get; set; }

        // The pulse mode (singlet = 0, doublet = 1, triplet = 2)
        public int PulseMode { get; set; }

        /// <summary>
        /// Default constructor for a waveform entry
        /// </summary>
        public Waveform()
        {
            Functionality = "Other";
            DefaultAmplitude = Constants.startupDefaultAmplitude;
            MinAmplitude = DefaultAmplitude - Constants.amplitudeRange;
            MaxAmplitude = DefaultAmplitude + Constants.amplitudeRange;
            PulseWidth = Constants.defaultPulseWidth;
            PulseMode = Constants.defaultPulseMode;
        }

        /// <summary>
        /// Custom constructor for a waveform entry
        /// </summary>
        public Waveform(string functionality)
        {
            Functionality = functionality;
            DefaultAmplitude = Constants.startupDefaultAmplitude;
            MinAmplitude = DefaultAmplitude - Constants.amplitudeRange;
            MaxAmplitude = DefaultAmplitude + Constants.amplitudeRange;
            PulseWidth = Constants.defaultPulseWidth;
            PulseMode = Constants.defaultPulseMode;
        }
    }
}