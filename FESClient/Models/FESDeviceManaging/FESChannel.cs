﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESClient.Models.FESDeviceManaging
{
    public class FESChannel
    {
        // An FES channel has an associated stim amplitude
        private int _amplitude;
        public int Amplitude
        {
            get
            {
                return _amplitude;
            }
            set
            {
                // Ensure that the amplitude is always within acceptable ranges (0 - 50 mA)
                if (value < Constants.minAmplitude)
                {
                    _amplitude = Constants.minAmplitude;
                }
                else if (value > Constants.maxAmplitude)
                {
                    _amplitude = Constants.maxAmplitude;
                }
                else
                {
                    _amplitude = value;
                }
            }
        }

        // An FES channel has an associated pulsewidth 
        private int _pulseWidth;
        public int PulseWidth
        {
            get
            {
                return _pulseWidth;
            }
            set
            {
                // Ensure that the pulse width is always within acceptable ranges (0, or 10 - 500 us)
                if (value == Constants.zeroPulseWidth)
                {
                    _pulseWidth = Constants.zeroPulseWidth;
                }
                else if (value > Constants.maxPulseWidth)
                {
                    _pulseWidth = Constants.maxPulseWidth;
                }
                else if (value < Constants.minPulseWidth)
                {
                    _pulseWidth = Constants.minPulseWidth;
                }
                else
                {
                    _pulseWidth = value;
                }
            }
        }

        // An FES channel has an associated pulse mode (singlet doublet triplet)
        private int _pulseMode;
        public int PulseMode
        {
            get
            {
                return _pulseMode;
            }
            set
            {
                // Ensure that the pulse mode always has a value of 0, 1, or 2
                if (value < Constants.minPulseMode)
                {
                    _pulseMode = Constants.minPulseMode;
                }
                else if (value > Constants.maxPulseMode)
                {
                    _pulseMode = Constants.maxPulseMode;
                }
                else
                {
                    _pulseMode = value;
                }
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public FESChannel()
        {
            // Set the default FES channel parameters
            Amplitude = Constants.startupDefaultAmplitude;
            PulseWidth = Constants.defaultPulseWidth;
            PulseMode = Constants.defaultPulseMode;
        }
    }
}
