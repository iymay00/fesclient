﻿using FESClient.Utilities;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Threading.Tasks;

namespace FESClient.Models.FESDeviceManaging
{
    public class FESDevice
    { 
        // An enumeration used to keep track of whether the FES device is paused or unpaused. The device always starts up in the PAUSED state.
        private enum State{ UNINITIALIZED, UNPAUSED, PAUSED};
        private State _state = State.UNINITIALIZED; 

        // In FES device communicates with the PC/Tablet via Serial interface
        SerialPort serialPort { get; set; }

        // "Channel List Mode," each channel of the FES device has associated: 
        //  - Amplitude (0-127 mA; resolution of 1 mA)
        //  - Pulsewidth (0, or 10 - 500 us; resolution 1 us)
        //  - Pulse mode (singlet {0}, doublet {1}, triplet {2})
        //  TODO: Extend this to support doublets/triplets
        private ObservableCollection<FESChannel> _channelSettings;
        public ObservableCollection<FESChannel> ChannelSettings
        {
            get
            {
                return _channelSettings;
            }
            set
            {
                // Update the value of the channel settings
                _channelSettings = value;

                // Upload the new channel settings to the FES device whenever the channel settings array is modified
                // TODO: Don't use get awaiter?
                WriteChannelSettings().GetAwaiter();
            }
        }

        // A cache that is used when pausing the stimulation to resume stim with the pre-paused parameters.
        private ObservableCollection<FESChannel> _channelSettingsCache;

        // A channel settings list for the FES device in the paused state. This is not modifiable.
        private readonly ObservableCollection<FESChannel> FESPauseSettings = new ObservableCollection<FESChannel>
        (
            new FESChannel[Constants.nChannels].Select(c => c = new FESChannel()).ToArray()
        );

        // The buffers used to initialize and shutdown the FES device
        private byte[] _initializationBuffer;
        private byte[] _shutdownBuffer;
        private readonly byte[] _finishedBuffer = { 192 };

        /// <summary>
        /// Constructor for the FES Device 
        /// </summary>
        public FESDevice(SerialPort serialPort, ObservableCollection<FESChannel> channelSettings)
        {
            // Set the device's serial port 
            this.serialPort = serialPort;

            // Initialize the channel settings array 
            ChannelSettings = channelSettings;

            // Initialize the cached channel settings array 
            _channelSettingsCache = new ObservableCollection<FESChannel>();

            // Compose the buffer to initialize & stop the FES device to Channel List Mode 
            ComposeChannelListModeBuffer();
            ComposeStopBuffer();

            // Initialiye the FES device 
            //Initialize().GetAwaiter();
        }


        public async Task Initialize()
        {
            await WriteFES(_initializationBuffer);
        }

        /// <summary>
        /// A method to update all the channels of the FES device 
        /// </summary>
        /// <param name="channelSettings"> An array of channel settings. </param>
        /// <returns>  </returns>
        public async Task WriteChannelSettings()
        {
            // If the FES device is paused, it should not receive any new commands
            if (_state == State.PAUSED)
            {
                return;
            }

            // Compose the amplitude and pulsewidth arrays by extracting them from the channel settings array 
            int[] amplitudes = ChannelSettings.Select(channel => channel.Amplitude).ToArray();
            int[] pulseWidths = ChannelSettings.Select(channel => channel.PulseWidth).ToArray();

            // Compose the bitstring
            string bitstring = FESUtilities.ComposeBitstring(amplitudes, pulseWidths);
            byte[] settings = FESUtilities.ToByteArray(bitstring);

            // Send the command to the FES
            string response = await WriteFES(settings);
        }

        /// <summary>
        /// Method to set all current amplitudes to zero in the FES device and stash the settings
        /// </summary>
        /// <returns></returns>
        public void Pause()
        {
            // Stash the channel settings in the cache for un-pausing later
            _channelSettingsCache = ChannelSettings;

            // Upload the pause settings to the FES device
            ChannelSettings = FESPauseSettings;

            // Update the state of the device 
            _state = State.PAUSED;
        }

        /// <summary>
        /// Resume the FES device's operation using the stashed channel settings
        /// </summary>
        public void Resume()
        {
            if (_state == State.PAUSED)
            {
                // Update the state of the device 
                _state = State.UNPAUSED;

                // Unstash the channel settings
                ChannelSettings = _channelSettingsCache;
            }
        }

        /// <summary>
        /// Stop the FES device
        /// </summary>
        public async void Stop()
        {
            await WriteFES(_shutdownBuffer);
        }

        private void ComposeChannelListModeBuffer(int stimulationFrequency = Constants.defaultStimulationFrequency,
                                                    int groupStimulationFrequency = Constants.defaultSGroupStimulationFrequency,
                                                    int nFactor = Constants.DefaultNFactor)
        {
            // Set ts1 and ts2
            int ts1 = 1000 / stimulationFrequency;
            int ts2 = 1000 / Constants.defaultSGroupStimulationFrequency;

            // Compute the main and group times
            int mainTime = (int)((ts1 - 1.0) / 0.5);
            int groupTime = (int)((ts2 - 1.5) / 0.5);

            // Set the active channels
            string activeChannelString = "11111111";

            // Specify which channels apply the scaler to get lower frequencies
            string lowFrequencyChannelString = "00000000";

            // Construct checksum
            int checksum = (nFactor + Convert.ToInt32(activeChannelString) + Convert.ToInt32(lowFrequencyChannelString) + groupTime + mainTime) % 8;

            // Convert parameters to their correct binary representation.
            string mainTimeBinary = Convert.ToString(mainTime, 2).PadLeft(11, '0');
            string nFactorBinary = Convert.ToString(nFactor, 2).PadLeft(3, '0');
            string groupTimeBinary = Convert.ToString(groupTime, 2).PadLeft(5, '0');
            string checksumBinary = Convert.ToString(checksum, 2).PadLeft(3, '0');

            // Construct the bit string 
            string bitString = "100" +                                          // 100
                               checksumBinary +                                 // Complete checksum
                               nFactorBinary.Substring(0, 2) +                  // First two bits of the N Factor                                     
                               "0" +                                            // 0
                               nFactorBinary.Substring(2, 1) +                  // Last (third) bit of the N factor
                               activeChannelString.Substring(0, 6) +            // First 6 bits of the active channel string
                               "0" +                                            // 0
                               activeChannelString.Substring(6, 2) +            // Last two bits of the active channel string
                               lowFrequencyChannelString.Substring(0, 5) +      // First 5 bits of the low frequency channel string
                               "0" +                                            // 0
                               lowFrequencyChannelString.Substring(5, 3) +      // Last 3 bits of the low frequency channel string
                               "00" +                                           // 00
                               groupTimeBinary.Substring(0, 2) +                // First two bits of the group time
                               "0" +                                            // 0
                               groupTimeBinary.Substring(2, 3) +                // Last three bits of the group time
                               mainTimeBinary.Substring(0, 4) +                 // First four bits of the main time
                               "0" +                                            // 0
                               mainTimeBinary.Substring(4, 7);                  // Last 7 bits of the main time

            // Update the initialization buffer
            _initializationBuffer = FESUtilities.ToByteArray(bitString);
        }

        private void ComposeStopBuffer()
        {
            // Compose the bitstring
            string bitString = "11000000";

            // Update the shutdown buffer
            _shutdownBuffer = FESUtilities.ToByteArray(bitString);
        }

        /// <summary>
        /// A method for writing byte array commands to the FES device when in Science Mode 
        /// </summary>
        /// <param name="command"> The byte array command to be sent to the FES device. </param>
        /// <returns> The byte array response sent by the FES device. </returns>
        private async Task<string> WriteFES(byte[] command)
        {
            // Write the command to the serial port asynchronously
            await serialPort.BaseStream.WriteAsync(command, 0, command.Length);
            await serialPort.BaseStream.FlushAsync();

            // Read the response from the FES device
            int readBufferSize = 1;
            byte[] readBuffer = new byte[readBufferSize];
            int returnValue = await serialPort.BaseStream.ReadAsync(readBuffer, 0, readBufferSize);

            // Check if the command was successful or not and return the appropriate success code
            if (returnValue % 2 == 0)
            {
                await serialPort.BaseStream.WriteAsync(_finishedBuffer, 0, _finishedBuffer.Length);
                await serialPort.BaseStream.FlushAsync();
                return "1";
            }
            else
            {
                return "0";
            }
        }
    }
}
