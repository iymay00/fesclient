﻿using FESClient.Models.FESDeviceManaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESClient.Models
{
    public class FESClientManager
    {
        // The FES client has a log manager 

        // The FES client has an HTTP server

        // The FES client has an HTTP request interpreter 

        // The FES client has an FES device
        public FESDevice fesDevice { get; set; }

        /// <summary>
        /// Custom constructor
        /// </summary>
        public FESClientManager(FESDevice fesDevice)
        {
            this.fesDevice = fesDevice;
        }
    }
}
