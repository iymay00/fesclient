﻿#pragma checksum "..\..\..\Views\TuningWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C643381E7C8FE2518B5E6FE572EBDEB5727E2010"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FESClient.Views;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace FESClient.Views {
    
    
    /// <summary>
    /// TuningWindow
    /// </summary>
    public partial class TuningWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 53 "..\..\..\Views\TuningWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button IncrementAmplitude;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Views\TuningWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DecrementAmplitude;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\Views\TuningWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EmergencyStopButton;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\Views\TuningWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveTuning;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\Views\TuningWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PauseResumeTuning;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\Views\TuningWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancelTuning;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FESClient;component/views/tuningwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\TuningWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\..\Views\TuningWindow.xaml"
            ((FESClient.Views.TuningWindow)(target)).Closed += new System.EventHandler(this.Window_Closed);
            
            #line default
            #line hidden
            return;
            case 2:
            this.IncrementAmplitude = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\..\Views\TuningWindow.xaml"
            this.IncrementAmplitude.Click += new System.Windows.RoutedEventHandler(this.IncrementAmplitude_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.DecrementAmplitude = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\..\Views\TuningWindow.xaml"
            this.DecrementAmplitude.Click += new System.Windows.RoutedEventHandler(this.DecrementAmplitude_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.EmergencyStopButton = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.SaveTuning = ((System.Windows.Controls.Button)(target));
            
            #line 111 "..\..\..\Views\TuningWindow.xaml"
            this.SaveTuning.Click += new System.Windows.RoutedEventHandler(this.SaveTuning_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.PauseResumeTuning = ((System.Windows.Controls.Button)(target));
            
            #line 122 "..\..\..\Views\TuningWindow.xaml"
            this.PauseResumeTuning.Click += new System.Windows.RoutedEventHandler(this.PauseResumeTuning_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.CancelTuning = ((System.Windows.Controls.Button)(target));
            
            #line 133 "..\..\..\Views\TuningWindow.xaml"
            this.CancelTuning.Click += new System.Windows.RoutedEventHandler(this.CancelTuning_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

