﻿using FESClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FESClient
{
    public static class Constants
    {
        // Log Path relative to the project 
        public const string logPath = "Logs\\";

        // Number of channels in the FES device 
        public const int nChannels = 8;

        // IP address for the FES client 
        public const string defaultIPAddress = "127.0.0.1";

        // Default FES starting parameters 
        public const int defaultStimulationFrequency = 20;
        public const int defaultSGroupStimulationFrequency = 50;
        public const int stimulationMode = 0;
        public const int DefaultNFactor = 0;
        public const int stimulationMaxAllowedCurrent = 50;

        // Constants for the FES device's amplitude parameter (including hard safety limits for the FES current output)
        public const int startupDefaultAmplitude = 0;
        public const int maxAmplitude = 50;
        public const int minAmplitude = 0;
        public const int amplitudeIncrements = 1;
        public const int amplitudeRange = 10;
        public const int startupTuningAmplitude = 0;

        // Constants for the FES device pulsewidth parameter 
        public const int defaultPulseWidth = 300;
        public const int maxPulseWidth = 500;
        public const int minPulseWidth = 10;
        public const int zeroPulseWidth = 0;
        public const int pulseWidthIncrement = 1;

        // Constants for the FES device pulse mode parameter 
        public const int defaultPulseMode = 0;
        public const int maxPulseMode = 2;
        public const int minPulseMode = 0;

        // Constants for the NRPA emulator 
        public const int nStimulationLines = 90;
        public const int nWaveformes = 10;
        public const int defaultStimulationLineDuration = 0;
        public const int defaultStimulationLineFrequency = 20;
        public const bool defaultWaveformEnableState = false;

        // The default waveform list that the FES client will display
        public static List<string> startupFunctionalities = new List<string>
        {
            "Left Flexion Synergy",        
            "Right Flexion Synergy",
            //"Left Hip Flexion", These were instead of the L/R synergies
            //"Left Knee Extension",
            "Left Knee Flexion",
            "Left Ankle Flexion",
            "Left Ankle Extension",
            "Right Hip Flexion",
            "Right Knee Extension",
            "Right Knee Flexion",
            "Right Ankle Flexion",
            "Other"
        };

        // The enabled functionalities
        public static List<string> enabledFunctionalities = new List<string>
        {
            "Left Knee Flexion",        
            "Left Knee Extension",
            "Left Ankle Flexion",
            "Left Ankle Extension",
            "Right Knee Flexion",
            "Right Knee Extension",
            "Right Ankle Flexion",
            "Right Ankle Extension",
            "Left Flexion Synergy",
            "Right Flexion Synergy"
        };
    }
}
